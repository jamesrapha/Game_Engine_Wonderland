﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour {

    private string[] messages = {"Use the arrow keys to zigzag between obstacles", "Jump with the space bar", "The best path isn't always the obvious one!"};
    int messageCounter = 0;

    private GameObject instructionPanel;


    void Awake() {
        instructionPanel = GameObject.Find("Instruction Panel");
        StartCoroutine(DisableGameObject(instructionPanel, 5));
    }

    IEnumerator DisableGameObject(GameObject obj, int sec) {
        yield return new WaitForSeconds(sec);
        obj.SetActive(false);

    }

    public void DisplayNextMessage() {
        instructionPanel.SetActive(true);

        GameObject.Find("Text").GetComponent<Text>().text = messages[messageCounter];
        messageCounter = (messageCounter + 1) % messages.Length;

        StartCoroutine(DisableGameObject(instructionPanel, 5));

    }


    public void LoadNextScene() {
        SceneManager.LoadScene("Freefall");
    }
}
