﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectable : MonoBehaviour {
    private GameObject GameController;

	// Use this for initialization
	void Start () {
	    GameController = GameObject.FindGameObjectsWithTag("GameController")[0];
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    
    void OnTriggerEnter(Collider other){
        string colorName = "None";
        if(other.tag == "Alice"){
            if(this.tag == "keyPart"){
                GameController.SendMessage("OnGetKey");
            } else if(this.tag == "key"){
                GameController.SendMessage("OnGetKey");
                GameController.SendMessage("OnGetColor", colorName, SendMessageOptions.RequireReceiver); 
            } else {
	            switch(this.name){
	                case "FairyLightBlue(Clone)":
	                    colorName = "Blue";
	                    break;
	                case "FairyLightRed(Clone)":
	                    colorName = "Red";
	                    break;
	                case "FairyLightOrange(Clone)":
	                    colorName = "Orange";
	                    break;
	                case "FairyLightGreen(Clone)":
	                    colorName = "Green";
	                    break;
	                case "FairyLightViolet(Clone)":
	                    colorName = "Violet";
	                    break;
	                default:
	                    Debug.Log("Not a valid Object");
	                    break;
	            }            
                GameController.SendMessage("OnGetColor", colorName, SendMessageOptions.RequireReceiver); 

            }
            Destroy(this.gameObject);
        }    
   
    }
}
