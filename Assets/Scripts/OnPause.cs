﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class OnPause : MonoBehaviour {

    public Transform Canvas;
    public Transform Player;

	
	// Update is called once per frame
	void Update () {
	
		if(Input.GetKeyUp(KeyCode.Escape)){
			Pause();
		}

	}

    public void Pause()
    {
 	if (Canvas.gameObject.activeInHierarchy == false && Cursor.lockState == CursorLockMode.None){
		Canvas.gameObject.SetActive(true);
		Time.timeScale = 0;
		Player.GetComponent<FirstPersonController>().enabled = false;
		//Cursor.lockState = CursorLockMode.None;
	} else {
		Canvas.gameObject.SetActive(false);
		Time.timeScale = 1; 
		Player.GetComponent<FirstPersonController>().enabled = true;
	}

    }
}
