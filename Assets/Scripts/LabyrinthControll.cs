﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;
using UnityEngine.SceneManagement;

public class LabyrinthControll : MonoBehaviour {
    public GameObject Alice;
    public GameObject camera;
    public GameObject Labyrinth;

    private GameObject MessageBottle;
    private GameObject MessageCake;
    private GameObject MessageDoor;
    private GameObject MessageDoorLocked;
    private GameObject HUDBottle;
    private GameObject HUDCake;
    private GameObject HUDKey;
    private GameObject InstructionPanel;

    public GameObject HUD;

    private bool hasBottle;
    private bool hasCake;
    private bool hasKey;

	// Use this for initialization
	void Start () {
		hasBottle = false;
        hasCake = false;
        hasKey = false;
        
        InstructionPanel = HUD.transform.Find("Instruction Panel").gameObject;
        MessageBottle = HUD.transform.Find("MessageBottle").gameObject;
        MessageCake = HUD.transform.Find("MessageCake").gameObject;
        MessageDoor = HUD.transform.Find("MessageDoor").gameObject;
        MessageDoorLocked = HUD.transform.Find("MessageDoorLocked").gameObject;


        HUDBottle = HUD.transform.Find("HUDBottle").gameObject;
        HUDCake = HUD.transform.Find("HUDCake").gameObject;
        HUDKey = HUD.transform.Find("HUDKey").gameObject;
	}

    public void OnGetKey(){
        Labyrinth.GetComponent<MazeLoader>().OnGetKey();
        hasKey = true;
        HUDKey.SetActive(true);
    }

    IEnumerator Resize(Transform t, Vector3 scale, float forwardSpeed, float backwardSpeed, float strafeSpeed, float jumpForce) {
        float progress = 0.0f;
        float lastProgress = 0.0f;

        bool justAsec = false;
        Vector3 initScale = t.localScale;

        float initPositionY = t.position.y;
        float initNearClipPlane = GameObject.Find("MainCamera").GetComponent<Camera>().nearClipPlane;

        while (progress <= 1.0f) {
            t.localScale = Vector3.Lerp(initScale, scale, progress);
            t.position = new Vector3(t.position.x, initPositionY * t.localScale.y / initScale.y, t.position.z);
            GameObject.Find("MainCamera").GetComponent<Camera>().nearClipPlane = initNearClipPlane * t.localScale.x / initScale.x;

            progress += Time.deltaTime * 0.4f;

            justAsec = (progress - lastProgress > 0.33f);

            if (justAsec) {
                lastProgress = progress;
                yield return new WaitForSeconds(0.5f);
            }
            else
                yield return null;
        }

        t.localScale = scale;
        t.position = new Vector3(t.position.x, initPositionY * scale.y / initScale.y, t.position.z);
        GameObject.Find("MainCamera").GetComponent<Camera>().nearClipPlane = initNearClipPlane * scale.x / initScale.x;

        if (scale.y < initScale.y) {
            Alice.GetComponent<CapsuleCollider>().radius = 0.1f;
            Alice.GetComponent<CapsuleCollider>().height = 6.0f;
            Alice.GetComponent<CapsuleCollider>().center = new Vector3(0.0f, 0.0f, 0.0f);
            Alice.transform.position = new Vector3(-1.14f, 2.22f, -0.56f);
        }
        else
            Alice.GetComponent<CapsuleCollider>().radius = 1.0f;

        Alice.GetComponent<AliceController>().movementSettings.ForwardSpeed = forwardSpeed;
        Alice.GetComponent<AliceController>().movementSettings.BackwardSpeed = backwardSpeed;
        Alice.GetComponent<AliceController>().movementSettings.StrafeSpeed = strafeSpeed;
        Alice.GetComponent<AliceController>().movementSettings.JumpForce = jumpForce;


    }
	
	// Update is called once per frame
	void Update () {
        // "s" moves Alice
        if (hasBottle && Input.GetKey("m")) {

            Alice.GetComponent<AliceController>().movementSettings.ForwardSpeed = 0.0f;
            Alice.GetComponent<AliceController>().movementSettings.BackwardSpeed = 0.0f;
            Alice.GetComponent<AliceController>().movementSettings.StrafeSpeed = 0.0f;
            Alice.GetComponent<AliceController>().movementSettings.JumpForce = 0.0f;

            Vector3 scale = new Vector3(0.05f, 0.05f, 0.05f);
            StartCoroutine(Resize(Alice.transform, scale, 1.0f, 1.0f, 1.0f, 7.0f));

        }

        else if (hasCake && Input.GetKey("p")) {

            Alice.GetComponent<AliceController>().movementSettings.ForwardSpeed = 0.0f;
            Alice.GetComponent<AliceController>().movementSettings.BackwardSpeed = 0.0f;
            Alice.GetComponent<AliceController>().movementSettings.StrafeSpeed = 0.0f;
            Alice.GetComponent<AliceController>().movementSettings.JumpForce = 0.0f;

            Vector3 scale = new Vector3(0.5f, 0.5f, 0.5f);
            StartCoroutine(Resize(Alice.transform, scale, 4.0f, 4.0f, 4.0f, 15.0f));


        }
	
        RaycastHit hitInfo;

        
        if(Physics.Raycast(camera.transform.position, camera.transform.forward, out hitInfo, 4.0f)){
                Debug.Log(hitInfo.collider.name);
	        if(hitInfo.collider.tag == "Bottle"){
	            MessageBottle.SetActive(true);
	            if(Input.GetKey("g")){
	                Destroy(hitInfo.collider.gameObject);
	                hasBottle = true;
                        MessageBottle.SetActive(false);
                        HUDBottle.SetActive(true);
	            }
	        } else if(hitInfo.collider.tag == "Cake"){
	            MessageCake.SetActive(true);
	            if(Input.GetKey("g")){
	                Destroy(hitInfo.collider.gameObject);
	                hasCake = true;
                        MessageCake.SetActive(false);
                        HUDCake.SetActive(true);
	            }
	        } else if (hitInfo.collider.tag == "Door") {
                    if (!MessageDoorLocked.activeSelf)
                        MessageDoor.SetActive(true);
                    if (Input.GetKey("o") || Input.GetKey("O")) {
                        if (hasKey) {
                            SceneManager.LoadScene("WorkInProgress");
                        }
                        else {
                            MessageDoor.SetActive(false);
                            MessageDoorLocked.SetActive(true);
                        }

                    }
                }               
                else {
                    MessageBottle.SetActive(false);
                    MessageCake.SetActive(false);
                    MessageDoor.SetActive(false);
                    MessageDoorLocked.SetActive(false);
            }
        } else {
                MessageBottle.SetActive(false);
                MessageCake.SetActive(false);
                MessageDoor.SetActive(false);
                MessageDoorLocked.SetActive(false);
        }   

        if(Time.timeSinceLevelLoad > 5.0f)
            InstructionPanel.SetActive(false);
    }
}
