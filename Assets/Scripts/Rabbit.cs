﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rabbit : MonoBehaviour {
    public float speed;
    public float distance;

    private Transform[] target;
    private int current = 0;
    private bool run = false;

    private Transform pivot;
    private Vector3 pivotStart;
    private Vector3 pivotVec;
    private float pivotNorm;

    // Use this for initialization
    void Start() {
        int count = GameObject.Find("Trajectory").transform.childCount;
        target = new Transform[count];

        for (int i = 0; i < count; i++)
            target[i] = GameObject.Find("Trajectory").transform.GetChild(i);
    }

    // Update is called once per frame
    void Update() {
        if (Mathf.Abs(transform.position.x - target[current].position.x) < 0.01 && Mathf.Abs(transform.position.z - target[current].position.z) < 0.01) {

            if (target[current].tag == "message")
                GameObject.Find("GameController").GetComponent<GameController>().DisplayNextMessage();

            if (target[current].tag == "fall")
                GameObject.Find("Rabbit").SetActive(false);

            current = Mathf.Min(current + 1, target.Length - 1);

            if (target[current].tag == "pivot") {
                pivot = target[current];
                current = Mathf.Min(current + 1, target.Length - 1);
                pivotNorm = Vector3.Distance(target[current].position, transform.position);
                pivotVec = Vector3.Normalize(target[current].position - transform.position);
                pivotStart = transform.position;
            }
            else
                pivot = null;

            run = (Vector3.Distance(transform.position, GameObject.Find("ThirdPersonController").transform.position) <= distance);

            if (run) {
                Vector3 pos;

                if (pivot != null) {
                    float coefTarget = Mathf.Min(1.0f, Vector3.Dot(transform.position - pivotStart, pivotVec) / pivotNorm * 2.0f);
                    float coefPivot = 1.0f - coefTarget;
                    //Debug.Log("coefPivot: "+coefPivot+" coefTarget: "+coefTarget);
                    pos = Vector3.MoveTowards(transform.position, target[current].position, speed * Time.deltaTime) * coefTarget + Vector3.MoveTowards(transform.position, pivot.position, speed * Time.deltaTime) * coefPivot;
                }
                else
                    pos = Vector3.MoveTowards(transform.position, target[current].position, speed * Time.deltaTime);

                GetComponent<Animator>().Play("Run");
                transform.LookAt(new Vector3(pos.x, 0.0f, pos.z));
                transform.position = pos;
            }
            else {
                GetComponent<Animator>().Play("Idle");
            }
        }

        else {
            if (run) {
                Vector3 pos;

                if (pivot != null) {
                    float coefTarget = Mathf.Min(1.0f, Vector3.Dot(transform.position - pivotStart, pivotVec) / pivotNorm * 2.0f);
                    float coefPivot = 1.0f - coefTarget;
                    //Debug.Log("coefPivot: " + coefPivot + " coefTarget: " + coefTarget);
                    pos = Vector3.MoveTowards(transform.position, target[current].position, speed * Time.deltaTime) * coefTarget + Vector3.MoveTowards(transform.position, pivot.position, speed * Time.deltaTime) * coefPivot;
                }
                else
                    pos = Vector3.MoveTowards(transform.position, target[current].position, speed * Time.deltaTime);

                GetComponent<Animator>().Play("Run");
                transform.LookAt(new Vector3(pos.x, 0.0f, pos.z));
                transform.position = pos;
            }
            else {
                GetComponent<Animator>().Play("Idle");
                run = (Vector3.Distance(transform.position, GameObject.Find("ThirdPersonController").transform.position) <= distance);
            }
        }

    }

}
