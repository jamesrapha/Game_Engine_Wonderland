﻿using UnityEngine;
using System.Collections;

public class MazeLoader : MonoBehaviour {
	public int mazeRows, mazeColumns;
	public GameObject wall;
    public GameObject roof;
	public float size = 2f;
    public float height = 2f;

    public GameObject exitWall;
    public GameObject key;

    private GameObject StoreExitWall;
	private MazeCell[,] mazeCells;

	// Use this for initialization
	void Start () {
		InitializeMaze ();

		MazeAlgorithm ma = new HuntAndKillMazeAlgorithm (mazeCells);
		ma.CreateMaze ();
	}
	
	// Update is called once per frame
	void Update () {
	}

    public void OnGetKey(){    
        StoreExitWall.GetComponent<Animation>().Play("Open");        
    }

	private void InitializeMaze() {
        Vector3 anchorPos = transform.position;
        transform.position = new Vector3(0,0,0);
		mazeCells = new MazeCell[mazeRows,mazeColumns];

		for (int r = 0; r < mazeRows; r++) {
			for (int c = 0; c < mazeColumns; c++) {
				mazeCells [r, c] = new MazeCell ();

				// For now, use the same wall object for the floor!
				mazeCells [r, c] .floor = Instantiate (wall, new Vector3 (r*size, -(height/2f), c*size), Quaternion.identity) as GameObject;
				mazeCells [r, c] .floor.name = "Floor " + r + "," + c;
				mazeCells [r, c] .floor.transform.Rotate (Vector3.right, 90f);
                mazeCells [r, c] .floor.transform.parent = transform;

				if (c == 0) {
					mazeCells[r,c].westWall = Instantiate (wall, new Vector3 (r*size, 0, (c*size) - (size/2f)), Quaternion.identity) as GameObject;
                    mazeCells[r,c].westWall.transform.localScale = new Vector3(size, height, 0.001f);
					mazeCells [r, c].westWall.name = "West Wall " + r + "," + c;
                    mazeCells [r, c] .westWall.transform.parent = transform;
				}

                if(r==mazeRows-1 && c==mazeColumns-1){
					StoreExitWall = Instantiate (exitWall, new Vector3 (r*size, -(height/2f), (c*size) + (size/2f)), Quaternion.identity) as GameObject;
	                StoreExitWall.transform.localScale = new Vector3(size, height, 0.001f);
					StoreExitWall.name = "Exit tWall " + r + "," + c;
	                StoreExitWall.transform.parent = transform;
                } else {
					mazeCells [r, c].eastWall = Instantiate (wall, new Vector3 (r*size, 0, (c*size) + (size/2f)), Quaternion.identity) as GameObject;
	                mazeCells[r,c].eastWall.transform.localScale = new Vector3(size, height, 0.001f);
					mazeCells [r, c].eastWall.name = "East Wall " + r + "," + c;
	                mazeCells [r, c] .eastWall.transform.parent = transform;
                }

				if (r == 0) {
					mazeCells [r, c].northWall = Instantiate (wall, new Vector3 ((r*size) - (size/2f), 0, c*size), Quaternion.identity) as GameObject;
                    mazeCells[r,c].northWall.transform.localScale = new Vector3(size, height, 0.001f);
					mazeCells [r, c].northWall.name = "North Wall " + r + "," + c;
					mazeCells [r, c].northWall.transform.Rotate (Vector3.up * 90f);
                    mazeCells [r, c] .northWall.transform.parent = transform;
				}

				mazeCells[r,c].southWall = Instantiate (wall, new Vector3 ((r*size) + (size/2f), 0, c*size), Quaternion.identity) as GameObject;
                mazeCells[r,c].southWall.transform.localScale = new Vector3(size, height, 0.001f);
				mazeCells [r, c].southWall.name = "South Wall " + r + "," + c;
				mazeCells [r, c].southWall.transform.Rotate (Vector3.up * 90f);
                mazeCells [r, c] .southWall.transform.parent = transform;
                 
                mazeCells [r, c] .roof = Instantiate (roof, new Vector3 (r*size, (height/2f), c*size), Quaternion.identity) as GameObject;
                mazeCells [r, c] .roof.name = "Roof " + r + "," + c;
                mazeCells [r, c] .roof.transform.Rotate (Vector3.right, -90f);
                mazeCells [r, c] .roof.transform.parent = transform;
			}
		}

        GameObject Key = Instantiate(key, new Vector3((mazeRows-1)*size, 0, (mazeColumns-1)*size), Quaternion.identity);
        Key.transform.localScale = new Vector3(0.01f,0.01f,0.06f);
        Key.transform.Rotate(Vector3.right, 90f);
        Key.transform.parent = transform;
        transform.position = anchorPos;
        Destroy(mazeCells [0,0].westWall);
	}
}
