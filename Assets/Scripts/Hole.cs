﻿using UnityEngine;
using System.Collections;

public class Hole : MonoBehaviour {

    IEnumerator LoadNextScene(int sec) {
        yield return new WaitForSeconds(sec);
        GameObject.Find("GameController").GetComponent<GameController>().LoadNextScene();

    }

    void OnCollisionEnter(Collision collision) {
        foreach (ContactPoint contact in collision.contacts) {
            if (contact.otherCollider.name == "ThirdPersonController") {
                contact.otherCollider.GetComponent<Collider>().enabled = false;
                contact.otherCollider.transform.position = new Vector3(GameObject.Find("Hole").transform.position.x, -0.5f, GameObject.Find("Hole").transform.position.z);
                contact.otherCollider.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ;
                contact.thisCollider.GetComponent<Renderer>().enabled = false;
                StartCoroutine(LoadNextScene(7));
            }
        }
    }
}
