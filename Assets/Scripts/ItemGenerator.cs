﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Shape {
    plane, circle
};

public class ItemGenerator : MonoBehaviour {
    public Shape Generator = 0; 
    public float Radius = 0.0f;
    public GameObject[] Generated_Objects;
    public float Lifetime = 5.0f;
    public int Emission_Rate = 1;
   

	// Use this for initialization
	void Start () {
        InvokeRepeating("InstantiateBySeconds", 2.0f, 1.0f/Emission_Rate);	
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void InstantiateBySeconds() {
        int random = Random.Range(0, Generated_Objects.Length);
        Vector3 pos = new Vector3(Random.Range(-Radius,Radius), transform.position.y, Random.Range(-Radius,Radius));
        Vector3 rot = new Vector3(Random.Range(0,360), Random.Range(0,360), Random.Range(0,360));
        GameObject floating_obj = Instantiate(Generated_Objects[random], pos, Quaternion.Euler(rot));
        Rigidbody physicBody = floating_obj.GetComponent<Rigidbody>();
        physicBody.useGravity = false;
        physicBody.velocity = new Vector3(0.0f,Random.Range(5.0f,15.0f),0.0f);
        floating_obj.transform.parent = this.transform;
        Destroy(floating_obj, Lifetime);
    }
}
