﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class FreeFallControll : MonoBehaviour {
    bool gameEnded;

    float StartTime = 0.0f;

    // Mirror to be controlled for gameflow
    public GameObject Mirror;
    public float introduction;

    private Vector3 StartPos, PlayPos;

    //Variables to manage color game
    private string requestedColor;
    private string[] possibleColors;
    private Color[] possibleColorsValue;
    private GameObject[] MirrorPart;
    private int level;
    private int currentStreak;
    private bool waitForKey;
    private Color basicColor = new Color(0.67f, 0.57f, 0.0f, 1.0f);

    private AudioSource audio;

    public int[] tryPerLevel = new int[3];

    public GameObject[] keyParts = new GameObject[3];
    public GameObject instructionPanel;
    public Text textPanel;
    public GameObject[] keyImages = new GameObject[3];

    public AudioClip fairyLightEffect;
    public AudioClip keyEffect;

    IEnumerator DisableGameObject(GameObject obj, int sec){
        yield return new WaitForSeconds(sec);
        obj.SetActive(false);
    }

    void ChangeText(){
        instructionPanel.SetActive(true);
        textPanel.text = "Oh ! The mirror changed color ...."; 
        StartCoroutine(DisableGameObject(instructionPanel, 5));
    }

	// Use this for initialization
	void Start () {
        gameEnded = false;
        waitForKey = false;
	    
        StartTime = Time.timeSinceLevelLoad;
        StartPos = Mirror.transform.position;
        PlayPos = new Vector3(StartPos.x, 0, StartPos.z);

        possibleColors = new string[]{"Red", "Blue", "Green", "Orange", "Violet"};
        possibleColorsValue = new Color[]{Color.red, Color.blue, Color.green, new Color(1.0f, 0.3f, 0.0f), new Color(0.7f, 0.1f, 0.7f)};     

        audio = GetComponent<AudioSource>();

        MirrorPart = GameObject.FindGameObjectsWithTag("MirrorPart");
        level = 0;

        Invoke("StartGame", 12);
        StartCoroutine(DisableGameObject(instructionPanel, 10));
        Invoke("ChangeText", 13);
    }

    void End(){
        SceneManager.LoadScene("Labyrinth"); 
    }

	void Update () {
        MirrorFlow();
    }

    void StartGame(){
        currentStreak = 0;
        audio.clip = fairyLightEffect;

        int idColor = Random.Range(0, possibleColorsValue.Length);
        requestedColor = possibleColors[idColor];

        //Change Mirror Light Color
        Color abc = possibleColorsValue[idColor];
        foreach(GameObject mirrorObject in MirrorPart)
            mirrorObject.GetComponent<Renderer>().material.SetColor("_Color", abc);        
    }

	
    void EndGame(){
        foreach(GameObject mirrorObject in MirrorPart)
            mirrorObject.GetComponent<Renderer>().material.SetColor("_Color", basicColor);
        Instantiate(keyParts[level], this.transform);
        waitForKey = true;
        level = level + 1;
    }

    void MirrorFlow(){
        if (Time.timeSinceLevelLoad < StartTime + introduction) {
            Mirror.transform.position = Vector3.Lerp(StartPos, PlayPos, Time.timeSinceLevelLoad/(StartTime+introduction));
	    } else {
            Mirror.transform.position = new Vector3(0,Mathf.Sin(Time.timeSinceLevelLoad)*1.0f,0);
            Mirror.transform.eulerAngles = new Vector3(Mathf.Sin(Time.timeSinceLevelLoad)*5.0f, Mathf.Cos(Time.timeSinceLevelLoad)*5.0f, Mathf.Sin(Time.timeSinceLevelLoad-10.0f)*5.0f);
        }
    }

    public void OnGetColor(string colorCollided){
        if(waitForKey == false && colorCollided == requestedColor){
            audio.Play();
            currentStreak = currentStreak + 1;
            
            // Condition to put to check if level completd, then drop key part, else increment said variable
            if(currentStreak == tryPerLevel[level]){
                EndGame();
            } else {
                int idColor = Random.Range(0, possibleColorsValue.Length);
                Color abc = possibleColorsValue[idColor];
                foreach(GameObject mirrorObject in MirrorPart)
                    mirrorObject.GetComponent<Renderer>().material.SetColor("_Color", abc);
                requestedColor = possibleColors[idColor];     
            }
        }
    }
    
    public void OnGetKey(){
        keyImages[level-1].SetActive(true);
        Debug.Log(level);
        audio.clip = keyEffect;
        audio.Play();
        if(level == tryPerLevel.Length){
            End();
            Debug.Log("Fini");
        } else {
            waitForKey = false;
            Invoke("StartGame", 2);
        }
    }
}
